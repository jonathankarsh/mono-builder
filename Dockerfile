FROM ubuntu:trusty
MAINTAINER jonathankarsh
# mono-builder: minimal docker container to build C# projects

# update
RUN apt-get -q update
# install basic requirements
RUN apt-get -q -y install curl gcc g++ make wget unzip
# install mono. done on a different line to allow the above to be cached separately
RUN apt-get -q -y install mono-complete

# use default parameters to ENTRYPOINT
# note data volumes expected mounted at /sources and /output
ADD build build
ENTRYPOINT ["./build"]
CMD ["/sources", "/output"]
